package com.goupan.book.chapter01;

/**
 * 描述:
 * <p>测试线程死锁
 * Created by goupan 2018/12/3 21:11
 */
public class Page_5_DeadLockDemo {

    private static String aa = "A";
    private static String bb = "B";

    public static void main(String[] args){
        new Page_5_DeadLockDemo().deadLock();
    }
    //40512 出现死锁的情况就不会打印3  一个等aa释放锁一个等bb释放锁
    public void deadLock(){
        Thread thread1 = new Thread(new Runnable() {
            public void run() {
                System.out.println("0");
                synchronized (aa){
                    try {
                        System.out.println("1");
                        Thread.currentThread().sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("2");
                    synchronized (bb){
                        System.out.println("3");
                    }
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            public void run() {
                System.out.println("4");
                synchronized (bb){
                    System.out.println("5");
                    synchronized (aa){
                        System.out.println("6");
                    }
                }
            }
        });
        thread1.start( );
        thread2.start();
    }

}
