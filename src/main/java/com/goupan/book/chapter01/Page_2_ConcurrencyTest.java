package com.goupan.book.chapter01;

/**
 * 描述:
 * <p>测试多线程是否一定比单线程快
 * Created by goupan 2018/12/3 20:25
 */
public class Page_2_ConcurrencyTest {

    private static final long count = 1001;

    public static void main(String[] args)throws InterruptedException {
        concurrency();
        serial();
    }

    public static void concurrency() throws InterruptedException {
        long start = System.currentTimeMillis();
        Thread thread = new Thread(new Runnable() {
            public void run() {
                int a = 0;
                for (int i = 0; i < count; i++) {
                    a += 5;
                }
            }
        });
        thread.start();
        int b = 0;
        for (long j = 0; j < count; j++) {
            b--;
        }
        thread.join();
        long time = System.currentTimeMillis() - start;
        System.out.println("concurrency: " + time + "ms b:" + b);
    }

    public static void serial() {
        long start = System.currentTimeMillis();
        int a = 0;
        for (int i = 0; i < count; i++) {
            a += 5;
        }

        int b = 0;
        for (long j = 0; j < count; j++) {
            b--;
        }

        long time = System.currentTimeMillis() - start;
        System.out.println("serial: " + time + "ms a:" +a+" b:"+b);
    }
}
