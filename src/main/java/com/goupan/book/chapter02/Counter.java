package com.goupan.book.chapter02;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 描述:使用CAS实现的线程安全的计数器代码
 * <p>
 * Created by goupan 2019/7/16 23:25
 */
public class Counter {


    private AtomicInteger atomicI = new AtomicInteger(0);
    private int num=0;

    /**  使用cas实现线程安全计数器 */
    private void  safeCount(){
        for (;;) {
            int num= atomicI.get();
            boolean suc = atomicI.compareAndSet(num,++num);
            if(suc){
                break;
            }
        }
    }

    /**  非线程安全计数器*/
    private void count(){
        num++;
    }


    public static void main(String[] args) {
        final Counter cas = new Counter();
        List<Thread> list = new ArrayList<Thread>(600);
        long start = System.currentTimeMillis();
        for (int j = 0; j < 100; j++) {
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    for (int i = 0; i < 10000; i++) {
                        cas.count();
                        cas.safeCount();
                    }
                }
            });
            list.add(thread);
        }

        for (Thread t:list) {
            t.start();
        }

        //等待所有线程执行完成
        for (Thread t:list) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("非线程安全计数器值为："+cas.num);
        System.out.println("线程安全计数器值为："+cas.atomicI.get());
        System.out.println("时间："+(System.currentTimeMillis()-start));
    }
}
