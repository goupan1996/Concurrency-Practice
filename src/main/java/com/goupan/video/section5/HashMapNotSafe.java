package com.goupan.video.section5;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * @author goupan
 * @date 2020/11/18 17:29
 *
 * @discription 演示hashmap扩容时，线程不安全的例子
 */
public class HashMapNotSafe {
    public static void main(String[] args) {
        final Map<Integer,String> map = new HashMap<>();

        // 65535
        final Integer targetKey = 0b1111_1111_1111_1111;
        final String targetValue = "存入的value";
        map.put(targetKey,targetValue);

        new Thread(()->{
            IntStream.range(0,targetKey).forEach(key->map.put(key,"值"));
        }).start();

        while(true){
            if(null == map.get(targetKey)){
                throw new RuntimeException("HashMap is not thread safe.");
            }
        }
    }
}
