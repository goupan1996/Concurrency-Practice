package com.goupan.video.section5;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author goupan
 * @date 2020/11/18 19:58
 *
 * @discription 演示CopyOnWriteArrayList迭代期间可以修改集合的内容
 */
public class CopyOnWriteArrayListDemo {

    public static void main(String[] args) {

        CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>(new Integer[]{1,2,3});
        System.out.println(list);

        Iterator<Integer> iterator1 = list.iterator();
        list.add(4);
        System.out.println(list);

        Iterator<Integer> iterator2 = list.iterator();
        System.out.println("====Verify Iterator 1 content====");
        iterator1.forEachRemaining(System.out::println);
        System.out.println("====Verify Iterator 2 content====");
        iterator2.forEachRemaining(System.out::println);
    }
}
