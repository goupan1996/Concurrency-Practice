package com.goupan.video.section5;

import java.util.HashMap;

/**
 * @author goupan
 * @date 2020/11/18 19:07
 *
 * @discription 演示hashmap结构从链表向红黑树的转换的情况
 */
public class HashMapDemo {

    public static void main(String[] args) {
        HashMap map = new HashMap<HashMapDemo,Integer>(1);
        for (int i = 0; i < 1000; i++) {
            HashMapDemo tmpHashMap = new HashMapDemo();
            map.put(tmpHashMap, null);
        }
        System.out.println("运行结束++++");
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
