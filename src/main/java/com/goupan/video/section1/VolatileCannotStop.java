package com.goupan.video.section1;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/24 17:56
 */
public class VolatileCannotStop {

    /**
     * 生产者
     */
    static class Producer implements Runnable{

        public volatile boolean canceled = false;
        BlockingQueue storage;

        public Producer(BlockingQueue storage) {
            this.storage = storage;
        }

        @Override
        public void run() {
            try{
                int num = 0;
                // 停止线程信号为true，且任务执行完毕
                while(num <= 100000 && !canceled){
                    if (num % 50 == 0) {
                        // 生产者队列满了之后，会使当前线程阻塞，阻塞后（被叫醒前）不能进入下一次循环，此时是感知不到canceled的状态的
                        storage.put(num);
                        System.out.println(num + "是50的倍数,被放到仓库中了。");
                    }
                    num++;
                }
            }catch (InterruptedException e){
                e.printStackTrace();
            }finally {
                System.out.println("生产者结束运行");
            }
        }
    }

    /**
     * 消费者
     */
    static class Consumer{

        BlockingQueue storage;

        public Consumer(BlockingQueue storage) {
            this.storage = storage;
        }
        public boolean needMoreNums() {
            if (Math.random() > 0.97) {
                return false;
            }
            return true;
        }
    }

    public static void main(String[] args) throws InterruptedException {

        ArrayBlockingQueue storage = new ArrayBlockingQueue(8);

        Producer producer = new Producer(storage);
        Thread producerThread = new Thread(producer);
        producerThread.start();
        Thread.sleep(500);

        Consumer consumer = new Consumer(storage);
        while (consumer.needMoreNums()) {
            System.out.println(consumer.storage.take() + "被消费了");
            Thread.sleep(100);
        }
        System.out.println("消费者不需要更多数据了。");

        //一旦消费不需要更多数据了，我们应该让生产者也停下来，但是实际情况却停不下来
        producer.canceled = true;
        System.out.println(producer.canceled);
    }
}
