package com.goupan.video.section1;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/24 17:15
 */
public class StopThread implements Runnable{

    @Override
    public void run() {
        int count = 0;
        // 退出循环表示中断标志位被设置为了true（有人想停止线程）或任务完成
        while(!Thread.currentThread().isInterrupted() && count < 1000){
            // 中断标记为false、且还有任务做则进入循环，每次都判断一下
            System.out.println("count = " + count++);
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(new StopThread());
        thread.start();
        // 执行当前语句的线程进行睡眠
        Thread.sleep(5);
        thread.interrupt();
    }
}
