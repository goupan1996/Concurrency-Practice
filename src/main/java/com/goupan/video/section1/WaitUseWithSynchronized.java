package com.goupan.video.section1;

import java.util.LinkedList;
import java.util.Queue;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/27 16:54
 */
public class WaitUseWithSynchronized {

    class BlockingQueue{

        Queue<String> buffer = new LinkedList<>();

        public void give(String data){
            synchronized (this){
                buffer.add(data);
                notify();
            }
        }

        public String take() throws InterruptedException {
            synchronized (this){
                while(buffer.isEmpty()){
                    wait();
                }
                return buffer.remove();
            }
        }
    }
}
