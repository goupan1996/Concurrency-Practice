package com.goupan.video.section1;

import java.util.Random;
import java.util.concurrent.*;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/23 17:43
 */
public class CallableTask implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        return new Random().nextInt();
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Future<Integer> future = executorService.submit(new CallableTask());
        System.out.println(future.get());
        System.out.println(future.isDone());
        System.out.println(future.isCancelled());
    }
}
