package com.goupan.video.section1;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/28 17:37
 */
public class WaitStyle {

    public static void main(String[] args) {
        MyBlockingQueue myBlockingQueue = new MyBlockingQueue(10);
        Producer producer = new Producer(myBlockingQueue);
        Consumer consumer = new Consumer(myBlockingQueue);
        new Thread(producer).start();
        new Thread(consumer).start();
    }

    static class Producer implements Runnable{

        private MyBlockingQueue myBlockingQueue;

        public Producer(MyBlockingQueue myBlockingQueue) {
            this.myBlockingQueue = myBlockingQueue;
        }
        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                try {
                    myBlockingQueue.put();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    static class Consumer implements Runnable{
        private MyBlockingQueue myBlockingQueue;

        public Consumer(MyBlockingQueue myBlockingQueue) {
            this.myBlockingQueue = myBlockingQueue;
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                try {
                    myBlockingQueue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
