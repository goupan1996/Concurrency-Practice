package com.goupan.video.section1;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/24 17:25
 */
public class StopDuringSleep implements Runnable{

    @Override
    public void run() {
        int num = 0;
        while(!Thread.currentThread().isInterrupted() && num <= 1000){
            try {
                System.out.println("num = "+num++);
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(new StopDuringSleep());
        thread.start();
        Thread.sleep(5);
        thread.interrupt();
    }
}
