package com.goupan.video.section1;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/27 17:56
 */
public class BlockingQueueExample {
    public static void main(String[] args) {

        BlockingQueue<Object> queue = new ArrayBlockingQueue<>(10);

        Runnable producer = ()->{
          while(true){
              try {
                  queue.put(new Object());
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
          }
        };
        // 二个生产者
        new Thread(producer).start();
        new Thread(producer).start();

        Runnable consumer = () -> {
          while(true){
              try {
                  queue.take();
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
          }
        };
        // 二个消费者消费
        new Thread(consumer).start();
        new Thread(consumer).start();

    }
}
