package com.goupan.video.section1;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/23 17:18
 */
public class RunnableThread  implements Runnable{
    @Override
    public void run() {
        System.out.println("用实现Runnable接口实现线程");
    }

    public static void main(String[] args) {
        Thread thread=new Thread(new RunnableThread());
        thread.start();
        innerClassThread();
        lambdaThread();
    }

    public static void innerClassThread(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("匿名内部类创建线程");
            }
        }).start();
    }

    public static void lambdaThread(){
       new Thread(()->{
           System.out.println("lambda表达式创建线程");
       }).start();
    }
}
