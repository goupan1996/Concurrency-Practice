package com.goupan.video.section1;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/27 18:05
 */
public class MyBlockingQueueForCondition {

    private Queue queue;
    private int queueMaxSize = 16;
    private ReentrantLock reentrantLock = new ReentrantLock();
    private Condition notEmpty = reentrantLock.newCondition();
    private Condition notFull = reentrantLock.newCondition();

    public MyBlockingQueueForCondition(int queueSize) {
        this.queueMaxSize = queueSize;
        queue = new LinkedList();
    }

    public void put(Object object){
        reentrantLock.lock();
        try{
            // 队列是否满了
            while(queue.size() == queueMaxSize){
                // 阻塞生产者线程并释放 Lock
                notFull.await();
            }
            queue.add(object);
            // 通知正在等待的所有消费者并唤醒它们
            notEmpty.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // 把 unlock 方法放在 finally 中是一个基本原则，否则可能会产生无法释放锁的情况
            reentrantLock.unlock();
        }
    }

    public Object take(){
        reentrantLock.lock();
        try{
            while(queue.size() == 0){
                // 已经空了 等待
                notEmpty.await();
            }
            Object item = queue.remove();
            notFull.signalAll();
            return item;
        }catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } finally {
            reentrantLock.unlock();
        }
    }
}
