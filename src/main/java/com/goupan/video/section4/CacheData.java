package com.goupan.video.section4;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author goupan
 * @date 2020/11/18 1:09
 *
 * @discription 读写锁降级功能代码演示
 */
public class CacheData {

    Object data;
    volatile boolean cacheValid;
    final ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();

    void processCacheData(){
        reentrantReadWriteLock.readLock().lock();
        if(!cacheValid){
            //在获取写锁之前，必须首先释放读锁。
            reentrantReadWriteLock.readLock().unlock();
            reentrantReadWriteLock.writeLock().lock();
            try{
                //这里需要再次判断数据的有效性,因为在我们释放读锁和获取写锁的空隙之内，可能有其他线程修改了数据。
                if(!cacheValid){
                    data = new Object();
                    cacheValid = true;
                }
                //在不释放写锁的情况下，直接获取读锁，这就是读写锁的降级。
                reentrantReadWriteLock.readLock().lock();
            }finally {
                //释放了写锁，但是依然持有读锁
                reentrantReadWriteLock.writeLock().unlock();
            }
        }
        try{
            System.out.println(data);
        }finally {
            reentrantReadWriteLock.readLock().unlock();
        }
    }
}
