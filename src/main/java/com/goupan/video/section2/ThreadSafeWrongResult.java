package com.goupan.video.section2;

/**
 * <P>  </P>
 *
 * @author Administrator
 * @since 2020/9/28 17:58
 */
public class ThreadSafeWrongResult {

    volatile static int  i = 0;
    public static void main(String[] args) throws InterruptedException {

        Runnable runnable = () -> {
            for (int j = 0; j < 10000; j++) {
                i++;
            }
        };
        Thread thread1 = new Thread(runnable);
        thread1.start();
        Thread thread2 = new Thread(runnable);
        thread2.start();

        // 主线程必须等待这两个线程执行完才能继续执行
        thread1.join();
        thread2.join();
        System.out.println(i);
    }
}
