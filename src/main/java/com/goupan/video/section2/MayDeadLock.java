package com.goupan.video.section2;

/**
 * <P> 产生死锁的情况</P>
 *
 * @author Administrator
 * @since 2020/9/28 17:58
 */
public class MayDeadLock {

    Object o1 = new Object();
    Object o2 = new Object();

    public void thread1() throws InterruptedException {
        synchronized (o1){
            Thread.sleep(500);
            synchronized (o2){
                System.out.println("线程1成功拿到两把锁");
            }
        }
    }

    public void thread2() throws InterruptedException{
        synchronized (o2){
            Thread.sleep(500);
            synchronized (o1){
                System.out.println("线程1成功拿到两把锁");
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MayDeadLock mayDeadLock = new MayDeadLock();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mayDeadLock.thread1();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mayDeadLock.thread2();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
