package com.goupan.video.section2;

import java.util.HashMap;
import java.util.Map;

/**
 * <P> 发布或初始化导致的线程安全问题 </P>
 *
 * @author Administrator
 * @since 2020/9/28 17:58
 */
public class WrongInit {

    private Map<Integer, String> students;

    public WrongInit() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                students = new HashMap<>();
                students.put(1, "王小美");
                students.put(2, "钱二宝");
                students.put(3, "周三");
                students.put(4, "赵四");
            }
        }).start();
    }

    public Map<Integer, String> getStudents() {
        return students;
    }

    public static void main(String[] args) throws InterruptedException {

        WrongInit multiThreadsError = new WrongInit();
        System.out.println(multiThreadsError.getStudents().get(1));
    }
}
