package com.goupan.video.section3;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author goupan
 * @date 2020/11/15 23:34
 *
 * @discription 支持定时或周期性执行任务的线程池
 */
public class ScheduledThreadPool {

    public static void main(String[] args) {
        ScheduledExecutorService service = Executors.newScheduledThreadPool(10);

        /**
         * schedule —— 比较简单表示延迟指定时间后执行一次任务，
         * 如果代码中设置参数为 10 秒，也就是 10 秒后执行一次任务后就结束。
         */
        service.schedule(new ThreadPoolDemo.Task(),10, TimeUnit.SECONDS);

        /**
         * scheduleAtFixedRate —— 表示以固定的频率执行任务，
         * 它的第二个参数 initialDelay 表示第一次延时时间，
         * 第三个参数 period 表示周期也就是第一次延时后每次延时多长时间执行一次任务。
         */
        service.scheduleAtFixedRate(new ThreadPoolDemo.Task(), 10, 10, TimeUnit.SECONDS);

        /**
         * scheduleWithFixedDelay —— 与第二种方法类似，也是周期执行任务，区别在于对周期的定义，
         * 之前的 scheduleAtFixedRate 是以任务开始的时间为时间起点，开始计时时间到就开始执行第二次任务，而不管任务需要花多久执行；
         * 而 scheduleWithFixedDelay 方法以任务结束的时间为下一次循环的时间起点开始计时。
         */
        service.scheduleWithFixedDelay(new ThreadPoolDemo.Task(), 10, 10, TimeUnit.SECONDS);
    }
}
