package com.goupan.video.section3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author goupan
 * @date 2020/11/13 0:33
 *
 * @discription 用固定线程数的线程池执行10000个任务 
 */
public class ThreadPoolDemo {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 10000; i++) {
            executorService.execute(new Task());
        }
        System.out.println("Thread Name: " + Thread.currentThread().getName());
    }
    static class Task implements Runnable{

        @Override
        public void run() {
            System.out.println("Thread Name: " + Thread.currentThread().getName());
        }
    }
}
