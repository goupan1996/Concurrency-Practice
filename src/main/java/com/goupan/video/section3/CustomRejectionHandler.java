package com.goupan.video.section3;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author goupan
 * @date 2020/11/14 23:33
 *
 * @discription 自定义线程池拒绝策略
 */
public class CustomRejectionHandler implements RejectedExecutionHandler {
    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        // 打印日志、暂存任务、重新执行等拒绝策略
    }
}
