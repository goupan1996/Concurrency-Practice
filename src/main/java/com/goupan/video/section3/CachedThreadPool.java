package com.goupan.video.section3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author goupan
 * @date 2020/11/15 23:17
 *
 * @discription 缓存线程池
 */
public class CachedThreadPool {

    public static void main(String[] args) {

        ExecutorService service = Executors.newCachedThreadPool();
        for (int i = 0; i < 1000; i++) {
            service.execute(new ThreadPoolDemo.Task());
        }
    }
    static class Task implements Runnable{
        @Override
        public void run() {
            System.out.println("Thread Name: " + Thread.currentThread().getName());
        }
    }
}
