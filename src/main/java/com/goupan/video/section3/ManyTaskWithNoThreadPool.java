package com.goupan.video.section3;

/**
 * @author goupan
 * @date 2020/11/13 0:16
 *
 * @discription 多任务不使用线程池的情况
 */
public class ManyTaskWithNoThreadPool {

    public static void main(String[] args) {

        for (int i = 0; i < 10000; i++) {
            Thread thread = new Thread(new Task());
            thread.start();
        }
    }

    static class Task implements Runnable{

        @Override
        public void run() {
            System.out.println("Thread Name: " + Thread.currentThread().getName());
        }
    }
}
